#docker-sf54-php74fpm-alpine-dev
FROM php:7.4-fpm-alpine
# https://hub.docker.com/_/php

# If you are having difficulty figuring out which Debian or Alpine packages need to be installed before docker-php-ext-install, then have a look at the install-php-extensions project. This script builds upon the docker-php-ext-* scripts and simplifies the installation of PHP extensions by automatically adding and removing Debian (apt) and Alpine (apk) packages. For example, to install the GD extension you simply have to run install-php-extensions gd. This tool is contributed by community members and is not included in the images, please refer to their Git repository for installation, usage, and issues.

# https://github.com/mlocati/docker-php-extension-installer
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions gd intl opcache pdo_pgsql pgsql pdo_mysql zip 

RUN install-php-extensions xdebug-3.1.6

RUN apk add --no-cache nginx certbot certbot-nginx

RUN apk add --no-cache bash git nodejs npm acl \
  && rm -rf /var/cache/apk/*

COPY ./nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./nginx/localhost.conf /etc/nginx/conf.d/localhost.conf
    
COPY ./php/my_php.ini "$PHP_INI_DIR/conf.d/"

RUN mkdir /www && mkdir /www/public
RUN echo "<?php echo phpinfo();" > /www/public/index.php

RUN echo "0       3       1       *       *      certbot renew --quiet" >> /var/spool/cron/crontabs/root


RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
RUN composer self-update
RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

COPY ./docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]


WORKDIR /www
